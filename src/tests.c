#define _DEFAULT_SOURCE
#include "tests.h"

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
static struct block_header* block_get_last(struct block_header* block) {
  struct block_header* last_block = block;
  while (last_block->next) last_block = last_block->next;
  return last_block;
}
static void* block_after(struct block_header const* block) {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd) {
  return (void*)snd == block_after(fst);
}
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }


void test_simple_malloc(struct block_header* block) {
  printf("---First test started---\n");
  void* block1 = _malloc(1000);
  if (!block1) err("Error! Block1 == NULL\n");
  printf("One block allocated\n");
  debug_heap(stdout, block);
  if (block->is_free) {
    _free(block1);
    err("Error! Block is free\n");
  }
  if (block->capacity.bytes != 1000) {
    _free(block1);
  	err("Error! Wrong size of block\n");
  }
  _free(block1);
  printf("OK!\n");
}

void test_simple_free(struct block_header* block) {
  printf("---Second test started---\n");
  void* block1 = _malloc(1000);
  void* block2 = _malloc(2000);
  void* block3 = _malloc(3000);
  if (!block1 || !block2 || !block3) err("Error! Some block is NULL\n");
  printf("Three blocks allocated\n");
  debug_heap(stdout, block);
  _free(block2);
  printf("Free block2\n");
  debug_heap(stdout, block);
  struct block_header* block2_header = block_get_header(block2);
  if (!block2_header->is_free) {
    _free(block1);
    _free(block3);
    err("Error! Block2 isn't free\n");
  }
  _free(block1);
  _free(block3);
  printf("OK!\n");
}

void test_two_blocks_free(struct block_header* block) {
  printf("---Third test started---\n");
  void* block1 = _malloc(1000);
  void* block2 = _malloc(2000);
  void* block3 = _malloc(3000);
  void* block4 = _malloc(4000);
  if (!block1 || !block2 || !block3 || !block4) err("Error! Some block is NULL\n");
  printf("Four blocks allocated\n");
  debug_heap(stdout, block);
  _free(block2);
  _free(block3);
  printf("Free block2 and block3\n");
  debug_heap(stdout, block);
  struct block_header* block2_header = block_get_header(block2);
  struct block_header* block3_header = block_get_header(block3);
  if (!block2_header->is_free || !block3_header->is_free) {
    _free(block1);
    _free(block3);
    err("Error! Block2 or block3 isn't free\n");
  }
  _free(block1);
  _free(block4);
  printf("OK!\n");
}

void test_new_region(struct block_header* block) {
  printf("---Fourth test started---\n");
  void* block1 = _malloc(20000);
  void* block2 = _malloc(1000);
  if (!block1 || !block2) err("Error! Some block is NULL\n");
  printf("Two blocks allocated\n");
  debug_heap(stdout, block);
  struct block_header* block1_header = block_get_header(block1);
  struct block_header* block2_header = block_get_header(block2);
  if (!blocks_continuous(block1_header, block2_header)) {
    _free(block1);
    _free(block2);
    err("Error! Block2 isn't allocated next to block1\n");
  }
  printf("Block2 is allocated next to block1\n");
  _free(block1);
  _free(block2);
  printf("OK!\n");
}

void test_new_region_in_new_place(struct block_header* block) {
  printf("---Fifth test started---\n");
  void* block1 = _malloc(20000);
  if (!block1) err("Error! Some block is NULL\n");
  printf("One big block allocated\n");
  debug_heap(stdout, block);
  struct block_header* last = block_get_last(block);
  map_pages((void *)round_pages((size_t) block_after(last)), 1000, MAP_FIXED_NOREPLACE);
  printf("Mapped memory after old region\n");
  printf("Allocated new block2\n");
  void* block2 = _malloc(10000);
  debug_heap(stdout, block);
  struct block_header* block1_header = block_get_header(block1);
  struct block_header* block2_header = block_get_header(block2);
  if (blocks_continuous(block1_header, block2_header)) {
    _free(block1);
    _free(block2);
    err("Error! Block2 is allocated next to block1\n");
  }
  printf("Block2 isn't allocated next to block1\n");
  _free(block1);
  _free(block2);
  printf("OK!\n");
}

void run_tests_set(){
  void* heap = heap_init(20000);
  if (!heap) err("Error in heap creating\n");
  debug_heap(stdout, heap);
  printf("%s\n", "Testing started successfully!");
  test_simple_malloc(heap);
  test_simple_free(heap);
  test_two_blocks_free(heap);
  test_new_region(heap);
  test_new_region_in_new_place(heap);
  printf("All tests passed successfully!\n");
}
