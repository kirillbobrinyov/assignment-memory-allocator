#ifndef _TESTS_H_
#define _TESTS_H_

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdio.h>
#include <unistd.h>

void run_tests_set();
void test_simple_malloc(struct block_header* block);
void test_simple_free(struct block_header* block);
void test_two_blocks_free(struct block_header* block);
void test_new_region(struct block_header* block);
void test_new_region_in_new_place(struct block_header* block);

#endif
